package se.plinc.snake.World;

import java.util.ArrayList;

import se.plinc.snake.InputHandler;
import se.plinc.snake.Snake;
import se.plinc.snake.Entities.Cloud;
import se.plinc.snake.Entities.Item;
import se.plinc.snake.Entities.SnakeHead;
import se.plinc.snake.Entities.SnakeSegment;
import se.plinc.snake.Screens.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class World {

	private ArrayList<SnakeSegment> snake;
	private Item item;
	private Cloud cloud;

	private Snake game;

	private Music music;
	
	private int score = 0;

	public final static int WIDTH = 800;
	public final static int HEIGHT = 450;

	public World(Snake game) {
		this.game = game;

		initComponents();
		initMusic();
		setInputListeners();
	}

	private void initComponents() {
		snake = new ArrayList<SnakeSegment>();

		snake.add(new SnakeHead(400, 300));
		snake.add(new SnakeSegment(432, 300));
		snake.add(new SnakeSegment(464, 300));
		snake.add(new SnakeSegment(496, 300));
		snake.add(new SnakeSegment(528, 300));

		cloud = new Cloud();
		item = new Item();
	}
	
	private void initMusic(){
		music = Gdx.audio.newMusic(Gdx.files
				.internal("data/snd/snaketheme.ogg"));
		music.setVolume(0.5f);
		music.play();
		music.setLooping(true);
	}
	
	private void setInputListeners(){
		Gdx.input.setInputProcessor(new InputHandler(this));
//		Gdx.input.setInputProcessor(new GestureHandler(new DirectionListener(
//				this)));
	}

	public void update() {
		updateSnakePosition();
		cloud.update();
	}

	public void updateSnakePosition() {

		for (int i = 0; i < snake.size(); i++) {
			if (i == 0) {
				SnakeHead head = (SnakeHead) snake.get(i);
				head.updatePosition();

				if (head.getBounds().overlaps(item.getBounds())) {
					item.setEaten(true);
					item.update();
					score += 100;
					snake.add(snake.size() - 1,
							new SnakeSegment(
									snake.get(snake.size() - 1).getX(), snake
											.get(snake.size() - 1).getY()));
				}
			}

			else if (i > 0) {

				if (snake.get(0).getBounds()
						.overlaps((snake.get(i).getBounds()))) {
					game.setScreen(new GameScreen(game));
				}

				snake.get(i).setLastX(snake.get(i).getX());
				snake.get(i).setLastY(snake.get(i).getY());
				snake.get(i).setX(snake.get(i - 1).getLastX());
				snake.get(i).setY(snake.get(i - 1).getLastY());

				if (i == 1 && snake.get(i).getX() < 0)
					snake.get(0).setX(World.WIDTH);
				else if (i == 1 && snake.get(i).getX() > World.WIDTH)
					snake.get(0).setX(0);
				else if (i == 1 && snake.get(i).getY() < 0)
					snake.get(0).setY(World.HEIGHT);
				else if (i == 1 && snake.get(i).getY() > World.HEIGHT)
					snake.get(0).setY(0);
			}
		}
	}

	public ArrayList<SnakeSegment> getSnake() {
		return snake;
	}

	public Item getItem() {
		return item;
	}

	public Cloud getCloud() {
		return cloud;
	}
	
	public int getScore(){
		return score;
	}

	public void dispose() {
		music.dispose();
	}
}
