package se.plinc.snake.World;

import java.util.ArrayList;

import se.plinc.snake.Entities.Cloud;
import se.plinc.snake.Entities.Item;
import se.plinc.snake.Entities.SnakeHead;
import se.plinc.snake.Entities.SnakeSegment;
import se.plinc.snake.Resources.SnakeImg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

public class WorldRenderer {

	private World world;
	private SnakeImg img;
	
	private Cloud cloud;

	private OrthographicCamera cam;
	private SpriteBatch batch;
	
	private BitmapFont font;
	private Label highScore;

	public WorldRenderer(World world) {
		this.world = world;
		img = new SnakeImg();
		
		initRendering();
		initScoreLabel();
	}
	
	private void initRendering(){
		cam = new OrthographicCamera();
		cam.setToOrtho(true, World.WIDTH, World.HEIGHT);
		cam.update();

		batch = new SpriteBatch();
		batch.setProjectionMatrix(cam.combined);
	}
	
	private void initScoreLabel(){
		font = new BitmapFont(Gdx.files.internal("data/fonts/snakefont.fnt"), true);

		LabelStyle ls = new LabelStyle(font, Color.WHITE);
		highScore = new Label("" + world.getScore(), ls);
		highScore.setY(20);
		highScore.setWidth(World.WIDTH);
		highScore.setAlignment(Align.center);
	}

	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		ArrayList<SnakeSegment> snake = world.getSnake();
		Item item = world.getItem();
		cloud = world.getCloud();
		highScore.setText("" + world.getScore());
		
		// Setup camera
		cam.position.set(World.WIDTH / 2, World.HEIGHT / 2, 0);
		cam.update();
		batch.setProjectionMatrix(cam.combined);

		// Begin rendering
		batch.begin();
		
		// Draw background
		batch.draw(SnakeImg.background, 0, 0, World.WIDTH, World.HEIGHT, 0, 0, SnakeImg.background.getWidth(), SnakeImg.background.getHeight(), false, false);
		
		// Draw shadows
		item.drawShadow(batch);
		for(int i = 0; i < snake.size(); i++){
			if(i == 0){
				SnakeHead head = (SnakeHead)snake.get(i);
				head.drawShadow(batch);
			}else{
				snake.get(i).drawShadows(batch);
			}
		}
		
		// Draw actual objects
		item.draw(batch);
		for(int i = 0; i < snake.size(); i++){
			if(i == 0){
				SnakeHead head = (SnakeHead)snake.get(i);
				head.draw(batch);
			}else{
				snake.get(i).drawBody(batch);
			}
		}
		
		cloud.draw(batch);
		highScore.draw(batch, 1);
		

		// End rendering
		batch.end();
	}

}
