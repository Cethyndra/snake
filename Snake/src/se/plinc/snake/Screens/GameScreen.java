package se.plinc.snake.Screens;

import se.plinc.snake.Snake;
import se.plinc.snake.World.World;
import se.plinc.snake.World.WorldRenderer;

import com.badlogic.gdx.Screen;

public class GameScreen implements Screen {

	Snake game;
	World world;
	WorldRenderer render;
	
	private float timer = 0;
	
	public GameScreen(Snake game){
		this.game = game;
		world = new World(game);
		render = new WorldRenderer(world);
	}
	
	@Override
	public void render(float delta) {
		timer += delta;
		if(timer > 0.1){
			world.update();
			timer = 0;
		}
		
		render.render();
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void hide() {
		world.dispose();
	}
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void dispose() {
		world.dispose();
	}
}
