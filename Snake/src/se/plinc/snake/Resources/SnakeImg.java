package se.plinc.snake.Resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class SnakeImg {
	
	public static Texture snakeSegment;
	public static Texture snakeHead;
	public static Texture headShadow;
	public static Texture bodyShadow;
	public static Texture item;
	public static Texture itemShadow;
	public static Texture background;
	public static Texture shadow;
	public static Texture cloud;
	
	public SnakeImg() {
		snakeSegment = new Texture(Gdx.files.internal("data/img/snake_segment.png"));
		snakeHead = new Texture(Gdx.files.internal("data/img/snake_head.png"));
		item = new Texture(Gdx.files.internal("data/img/itemnew.png"));
		itemShadow = new Texture(Gdx.files.internal("data/img/item_shadow.png"));
		bodyShadow = new Texture(Gdx.files.internal("data/img/shadow.png"));
		background = new Texture(Gdx.files.internal("data/img/back.png"));
		headShadow  = new Texture(Gdx.files.internal("data/img/head_shadow.png"));
		cloud = new Texture(Gdx.files.internal("data/img/cloud.png"));
	}
	
	public void dispose(){
		snakeSegment.dispose();
		item.dispose();
		background.dispose();
		bodyShadow.dispose();
		snakeHead.dispose();
		itemShadow.dispose();
		headShadow.dispose();
		cloud.dispose();
	}
}
