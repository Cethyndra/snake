package se.plinc.snake;

import se.plinc.snake.Entities.SnakeHead;
import se.plinc.snake.World.World;

public class DirectionListener {

	private World world;
	private SnakeHead head;

	public DirectionListener(World world) {
		this.world = world;
		head = (SnakeHead) world.getSnake().get(0);
	}

	public void onLeft() {
		System.out.println("WOOP");
		head.setUserInput(SnakeHead.LEFT);
	}

	void onRight() {
		head.setUserInput(SnakeHead.RIGHT);
	}

	void onUp() {
		head.setUserInput(SnakeHead.UP);
	}

	void onDown() {
		head.setUserInput(SnakeHead.DOWN);
	}

}
