package se.plinc.snake;

import se.plinc.snake.Entities.SnakeHead;
import se.plinc.snake.World.World;
import sun.rmi.runtime.Log;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class InputHandler implements InputProcessor {
	
	private World world;
	
	private SnakeHead head;
	
	public InputHandler(World world) {
		this.world = world;
		this.head = (SnakeHead)world.getSnake().get(0);
	}

	@Override
	public boolean keyDown(int keycode) {
		switch(keycode){
		case Keys.RIGHT:
			head.setUserInput(SnakeHead.RIGHT);
			break;
		case Keys.LEFT:
			head.setUserInput(SnakeHead.LEFT);
			break;
			
		case Keys.UP:
			head.setUserInput(SnakeHead.UP);
			break;
		
		case Keys.DOWN:
			head.setUserInput(SnakeHead.DOWN);
			break;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
