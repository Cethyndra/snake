package se.plinc.snake.Entities;

import java.util.Random;

import se.plinc.snake.Resources.SnakeImg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Item {

	private int x;
	private int y;
	private int width;
	private int height;
	private boolean isEaten;

	private Random rand;
	private Rectangle bounds;
	
	private Sound sound;

	public Item() {
		width = 32;
		height = 32;

		rand = new Random();
		x = (rand.nextInt(38) * 20); // Makes the item appear inside
		y = (rand.nextInt(21) * 20); // the drawn borders.

		bounds = new Rectangle(x, y, width, height);
		
		sound = Gdx.audio.newSound(Gdx.files.internal("data/snd/itemeaten.ogg"));
	}

	public void update() {
		if (isEaten) {
			sound.play();
			isEaten = false;
			x = (rand.nextInt(38) * 20); // Makes the item appear inside
			y = (rand.nextInt(21) * 20); // the drawn borders.
			bounds.set(x, y, width, height);
		}
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isEaten() {
		return isEaten;
	}

	public void setEaten(boolean isEaten) {
		this.isEaten = isEaten;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public void draw(SpriteBatch batch) {
		batch.draw(SnakeImg.item, x, y, width, height, 0, 0,
				SnakeImg.item.getWidth(), SnakeImg.item.getHeight(), false,
				true);
	}

	public void drawShadow(SpriteBatch batch) {
		batch.draw(SnakeImg.itemShadow, x - 4, y + 5, width, height, 0, 0,
				SnakeImg.itemShadow.getWidth(),
				SnakeImg.itemShadow.getHeight(), false, true);
	}

}
