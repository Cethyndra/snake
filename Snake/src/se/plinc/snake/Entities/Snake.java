package se.plinc.snake.Entities;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Snake {

	public static final int SEG_WIDTH = 32;
	public static final int SEG_HEIGHT = 32;

	private SnakeHead head;
	private ArrayList<SnakeSegment> body;

	public Snake(int startX, int startY, int length) {
		this.body = new ArrayList<SnakeSegment>();

		createSnake(startX, startY, length);
	}

	private void createSnake(int startX, int startY, int length) {
		head = new SnakeHead(startX, startY);

		for (int i = 1; i <= length; i++) {
			body.add(new SnakeSegment(startX + (length * SEG_WIDTH), startY));
		}
	}

	public SnakeHead getHead() {
		return head;
	}

	public void update() {

	}

	public boolean hasEatenItem(Item item) {
		if (head.getBounds().overlaps(item.getBounds()))
			return true;
		else
			return false;
	}

	public void grow() {
		int x = body.get(body.size() - 1).getX();
		int y = body.get(body.size() - 1).getY();
		body.add(body.size() - 1, new SnakeSegment(x, y));
	}
	
	public void draw(SpriteBatch batch){
		head.draw(batch);
		
		for (int i = 0; i < body.size() - 1; i++) {
			body.get(i).drawBody(batch);
		}
	}
	
	public void drawShadow(SpriteBatch batch){
		head.drawShadow(batch);
		
		for (int i = 0; i < body.size() - 1; i++) {
			body.get(i).drawShadows(batch);
		}
	}

}
