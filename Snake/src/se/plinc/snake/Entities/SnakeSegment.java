package se.plinc.snake.Entities;


import se.plinc.snake.Resources.SnakeImg;
import se.plinc.snake.Entities.SnakeHead;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class SnakeSegment {

	protected int x;
	protected int y;
	private int lastX;
	private int lastY;
	protected int width = 32;
	protected int height = 32;

	private Rectangle bounds;

	public SnakeSegment(int x, int y) {
		this.x = x;
		this.y = y;

		bounds = new Rectangle(x, y, width, height);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getLastX() {
		return lastX;
	}

	public void setLastX(int lastX) {
		this.lastX = lastX;
	}

	public int getLastY() {
		return lastY;
	}

	public void setLastY(int lastY) {
		this.lastY = lastY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Rectangle getBounds() {
		bounds.set(x, y, width, height);
		return bounds;
	}
	
	public void drawShadows(SpriteBatch batch){
		batch.draw(SnakeImg.bodyShadow, x - 4, y + 5, width, height, 0, 0, SnakeImg.bodyShadow.getWidth(), SnakeImg.bodyShadow.getHeight(), false, true);
	}
	
	public void drawBody(SpriteBatch batch){
		batch.draw(SnakeImg.snakeSegment, x, y, width, height, 0, 0, SnakeImg.snakeSegment.getWidth(), SnakeImg.snakeSegment.getHeight(), false, true);
	}

}
