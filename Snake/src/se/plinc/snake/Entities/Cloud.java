package se.plinc.snake.Entities;

import java.util.Random;

import se.plinc.snake.Resources.SnakeImg;
import se.plinc.snake.World.World;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Cloud {
	
	private int x, y, dx, width, height;
	private Random rand;
	
	public Cloud() {
		this.rand = new Random();
		this.dx = 2;
		this.x = World.WIDTH;
		this.y = rand.nextInt(World.HEIGHT);
		this.width = 512;
		this.height = 256;
	}
	
	public void update(){
		if((this.x + width) < 0){
			this.x = World.WIDTH;
			this.y = rand.nextInt(World.HEIGHT);
		}else{
			this.x -= dx;
		}
	}
	
	public void draw(SpriteBatch batch){
		batch.draw(SnakeImg.cloud, x, y, width, height, 0, 0, SnakeImg.cloud.getWidth(), SnakeImg.cloud.getHeight(), false, true);
	}
}
