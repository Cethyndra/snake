package se.plinc.snake.Entities;

import se.plinc.snake.Resources.SnakeImg;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SnakeHead extends SnakeSegment {

	private int dx;
	private int dy;
	private boolean movingRight;
	private boolean movingLeft;
	private boolean movingUp;
	private boolean movingDown;
	public static int direction;
	public static int userInput;

	public static final int UP = 0;
	public static final int DOWN = 180;
	public static final int LEFT = 270;
	public static final int RIGHT = 90;

	private final int SPEED = 32;

	public SnakeHead(int x, int y) {
		super(x, y);
		resetMovement();
		setMovementDirection(LEFT);
		setUserInput(LEFT);
	}

	public void updatePosition() {
		super.setLastX(super.getX());
		super.setLastY(super.getY());

		getMovementDirection();

		super.setX(super.getX() + dx);
		super.setY(super.getY() + dy);
	}
	
	public void setUserInput(int direction){
		this.userInput = direction;
	}

	private void setMovementDirection(int direction) {
		this.direction = direction;
	}

	public void getMovementDirection() {
		switch (userInput) {
		case UP:
			moveUp();
			break;
		case DOWN:
			moveDown();
			break;
		case LEFT:
			moveLeft();
			break;
		case RIGHT:
			moveRight();
			break;
		}
	}

	private void moveUp() {
		if (!movingDown) {

			dx = 0;
			dy = -SPEED;

			setMovementDirection(UP);
			resetMovement();
			movingUp = true;
		}
	}

	private void moveDown() {
		if (!movingUp) {

			dx = 0;
			dy = SPEED;

			setMovementDirection(DOWN);
			resetMovement();
			movingDown = true;
		}
	}

	private void moveLeft() {
		if (!movingRight) {

			dy = 0;
			dx = -SPEED;

			setMovementDirection(LEFT);
			resetMovement();
			movingLeft = true;
		}
	}

	private void moveRight() {
		if (!movingLeft) {

			dy = 0;
			dx = SPEED;

			setMovementDirection(RIGHT);
			resetMovement();
			movingRight = true;
		}
	}

	private void resetMovement() {
		movingUp = false;
		movingDown = false;
		movingRight = false;
		movingLeft = false;
	}

	public int getDx() {
		return dx;
	}

	public int getDy() {
		return dy;
	}

	public void draw(SpriteBatch batch) {
		batch.draw(SnakeImg.snakeHead, x, y, 0 + Math.abs(width / 2),
				0 + Math.abs(height / 2), width, height, 1, 1, direction, 0, 0,
				SnakeImg.snakeHead.getWidth(), SnakeImg.snakeHead.getHeight(),
				false, false);
	}

	public void drawShadow(SpriteBatch batch) {
		batch.draw(SnakeImg.headShadow, x - 4, y + 5, 0 + Math.abs(width / 2),
				0 + Math.abs(height / 2), width, height, 1, 1, direction, 0, 0,
				SnakeImg.headShadow.getWidth(),
				SnakeImg.headShadow.getHeight(), false, false);
	}

}
